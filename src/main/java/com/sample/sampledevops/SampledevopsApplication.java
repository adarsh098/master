package com.sample.sampledevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampledevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampledevopsApplication.class, args);
	}

}
